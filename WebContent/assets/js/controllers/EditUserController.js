angular.module('istarcms').controller('CreateUserController', function ($scope, $rootScope, $state, BackendService, UserService, $state) {
   /* $rootScope.setting.layout.pageWithoutHeader = true;
    $rootScope.setting.layout.paceTop = true;
    $rootScope.setting.layout.pageBgWhite = true;
*/
    $scope.doCreateUser = function () {
        var params = {
        		 username:$scope.username,
        		 email: $scope.useremail,
                 password: $scope.userpass,
                 phonenumber:$scope.phonenumber,
                 gender:$scope.gender
           
           
        };

        BackendService.doCommunication(BackendService.API.nativeEditUser, params, "GET").then(function (data) {
            var result = BackendService.parseBackendResponse(data);
            if (result.type == BackendService.toasterFriendlyCode.S) {
                UserService.setUserToken(result.data.token).then(function (data) {
                    UserService.setUserProfileInfo().then(function (userData) {
                        $state.go('member.editUser');
                    });
                });
            } else if (result.type == BackendService.toasterFriendlyCode.F) {
                alert(result.summaryMessages + " : " + result.detailedMessage);
            }
        }, function (err) {
            var result = BackendService.parseBackendResponse(err);
            if (result.type == BackendService.toasterFriendlyCode.F) {
                alert(result.summaryMessages + " : " + result.detailedMessage);
            }
        });

    };
});
