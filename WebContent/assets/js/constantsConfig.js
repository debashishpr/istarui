'use strict';

/**
 * Istar App Config constant
 */

istarcms.constant('ISTAR_CONFIG_CONSTANTS', {
    //*** Scripts
    scripts: {

        //*** Controllers
        'loginPageCtrl': 'assets/js/controllers/LoginPageController.js',
        'uHPCtrl': 'assets/js/controllers/UserHomePageController.js',
        'headerCtrl':'assets/js/controllers/HeaderController.js',
        'forgotpwdctrl':"assets/js/controllers/ForgotPasswordController.js",
        "forgotrstctrl": "assets/js/controllers/ForgotResetController.js",
        'entlstctrl': "assets/js/controllers/EntityListController.js",
        'createOrgCtrl': 'assets/js/controllers/OrganizationController.js',
        //*** Services
        'bkndserv': 'assets/js/services/BackendServices.js',
        'apis': 'assets/js/services/APIs.js',

        'onbserv': "assets/js/services/OnboardingServices.js",
    },
    //*** angularJS Modules
    modules: [{}]
});